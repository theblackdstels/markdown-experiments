# Markdown Experiments

Markdown experiments for FE training.
Gitlab is a web based source code management and CI/CD system.
I'm really enjoying learning how it works!

## Learning Goals
1. Have Fun
2. Learn Gitlab
3. ...
4. Profit!
5. Retire
6. Spend days *relaxing on the beach*
7. Learn to scuba dive
8. Create the perfect pina colada

**Bold section**
